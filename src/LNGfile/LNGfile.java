package LNGfile;

import jgp.std.JGPMath;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class LNGfile {
	private short languageCode; // wg standardu ISO 3166-1
	private int stringCount;
	private int stringAddress;
	private String languageName;
	private String languageLocalName;
	ArrayList<LNGstring> stringList;

	public LNGfile() {
		stringList = new ArrayList<LNGstring>();
	}
	public LNGfile(String path) {
		stringList = new ArrayList<LNGstring>();
		readFile(path);
	}

	private void setLanguageCode(String language) {
		if(language.equals("English")) {
			languageCode = 840;
			languageName = language;
			languageLocalName = language;
		}
		if(language.equals("Polski")) {
			languageCode = 616;
			languageName = "Poland";
			languageLocalName = language;
		}
		if(language.equals("�e�tina")) {
			languageCode = 203;
			languageName = "Czech";
			languageLocalName = language;
		}
		if(language.equals("Deutsch")) {
			languageCode = 276;
			languageName = "German";
			languageLocalName = language;
		}
		if(language.equals("nieokre�lony")) {
			languageCode = 0;
			languageName = "indefinite";
			languageLocalName = "indefinite";
		}
	}
	public String getLanguageLocalName() {
		return languageLocalName;
	}
	public String getString(int id) {
		return stringList.get(id).getString();
	}
	public void setString(int id, String str) {
		stringList.get(id).setString(str);
	}
	public int getStringCount() {
		return stringCount;
	}
	public void addLNGstring() {
		stringList.add(new LNGstring());
		stringCount++;
	}

	public void parsingData() {
		int currPos = 10 + JGPMath.stringByteLength(languageName) + JGPMath.stringByteLength(languageLocalName); // omini�cie warto�ci przed nag�owkiem
		currPos += (4 * stringCount); // omini�cie nag��wka
		for(int i = 0; i < stringCount; i++) {
			stringList.get(i).setTextAddress(currPos);
			currPos += JGPMath.stringByteLength(stringList.get(i).getString());
		}
		stringAddress = stringList.get(0).getTextAddress();
	}
	public void readFile(String path) {
		try {
			File file = new File(path);
			FileInputStream fs = new FileInputStream(file);
			DataInputStream ds = new DataInputStream(fs);
			languageCode = ds.readShort();
			stringCount = ds.readInt();
			stringAddress = ds.readInt();
			languageName = ds.readUTF();
			languageLocalName = ds.readUTF();
			
			//konwersja powy�ej odczytanych liczb
			languageCode = JGPMath.little2big(languageCode);
			stringCount = JGPMath.little2big(stringCount);
			stringAddress = JGPMath.little2big(stringAddress);
			
			// utworzenie obiekt�w LNGstring i zapisanie do listy stringList
			for(int i = 0; i < stringCount; i++)
				stringList.add(new LNGstring());
			// odczytywanie nag��wk�w string�w
			for(int i = 0; i < stringCount; i++)
				stringList.get(i).readHeader(ds);
			// odczytywanie string�w
			for(int i = 0; i < stringCount; i++)
				stringList.get(i).readString(ds);
			ds.close();
			fs.close();
		} catch (FileNotFoundException ex) {
			JOptionPane.showMessageDialog(null, "Plik: " + path + " nie istnieje!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException ex) {
			if(ex.getMessage() != null)
				JOptionPane.showMessageDialog(null, "IO exception!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	public void writeFile(String path, String language) {
		try {
			if(language != null)
				setLanguageCode(language);
			parsingData();
			
			File file = new File(path);
			FileOutputStream fs = new FileOutputStream(file);
			DataOutputStream ds = new DataOutputStream(fs);
			ds.writeShort(JGPMath.little2big(languageCode));
			ds.writeInt(JGPMath.little2big(stringCount));
			ds.writeInt(JGPMath.little2big(stringAddress));
			ds.writeUTF(languageName);
			ds.writeUTF(languageLocalName);
			
			// zapis nag��wk�w string�w
			for(int i = 0; i < stringCount; i++)
				stringList.get(i).writeHeader(ds);
			// zapis string�w
			for(int i = 0; i < stringCount; i++)
				stringList.get(i).writeString(ds);
			ds.close();
			fs.close();
		} catch (FileNotFoundException ex) {
			JOptionPane.showMessageDialog(null, "Plik: " + path + " nie istnieje!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException ex) {
			if(ex.getMessage() != null)
				JOptionPane.showMessageDialog(null, "IO exception!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, "B�ad w zapisie pliku: " + path + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	public void deleteString(int id) {
		stringList.remove(id);
		stringCount--;
	}
}
