package LNGfile;

import jgp.std.JGPMath;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class LNGstring {
	private int textAddress;
	private String text;
	
	public int getTextAddress() {
		return textAddress;
	}
	public void setTextAddress(int textAddress) {
		this.textAddress = textAddress;
	}
	public String getString() {
		return text;
	}
	public void setString(String text) {
		this.text = text;
	}
	
	public void readHeader(DataInputStream ds) throws IOException {
		textAddress = ds.readInt();
		textAddress = JGPMath.little2big(textAddress);
	}	
	public void readString(DataInputStream ds) throws IOException {
		text = ds.readUTF();
	}
	public void writeHeader(DataOutputStream ds) throws IOException {
		ds.writeInt(JGPMath.little2big(textAddress));
	}
	public void writeString(DataOutputStream ds) throws IOException {
		ds.writeUTF(text);
	}
}
