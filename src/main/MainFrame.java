package main;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import LNGfile.LNGfile;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = -6568109341839829439L;
	
	private JPanel panel;
	private JTable table;
	private JScrollPane tableScroll;
	private JMenuItem optionNew;
	private JMenuItem optionOpen;
	private JMenuItem optionSave;
	private JMenuItem optionSaveAs;
	private JMenuItem optionExit;
	private JCheckBox drawEndsString;
	private DefaultTableModel tableModel;
	private LNGfile lngFile;
	private String currentFilePath;
	private String defaultFrameTitle;

	JComboBox<String> lngCombo;

	public static final int ID_COLUMN = 0;
		public static final int MAX_ID_COL_WIDTH = 50;
		public static final int PREF_ID_COL_WIDTH = 35;
	public static final int STRING_COLUMN = 1;

	MainFrame(String title) {
		super(title);
		defaultFrameTitle = title;

		JMenuBar menuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("Plik");
		optionNew = new JMenuItem("Nowy");
		optionOpen = new JMenuItem("Otw�rz");
		optionSave = new JMenuItem("Zapisz");
		optionSaveAs = new JMenuItem("Zapisz jako...");
		optionExit = new JMenuItem("Zako�cz");
		
		optionNew.addActionListener(new NewItemListener());
		optionOpen.addActionListener(new OpenItemListener());
		optionSave.addActionListener(new SaveItemListener());
		optionSaveAs.addActionListener(new SaveAsItemListener(this));
		optionExit.addActionListener(new ExitItemListener(this));
		fileMenu.add(optionNew);
		fileMenu.add(optionOpen);
		fileMenu.add(optionSave);
		fileMenu.add(optionSaveAs);
		fileMenu.add(optionExit);
		menuBar.add(fileMenu);
		this.setJMenuBar(menuBar);
		optionSave.setEnabled(false);
		optionSaveAs.setEnabled(false);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(480, 530);
		this.setVisible(true);
	}

	// JMenuItem
	class ExitItemListener implements ActionListener {
		MainFrame frame;
		private ExitItemListener(MainFrame frame) {
			this.frame = frame;
		}
		public void actionPerformed(ActionEvent ev) {
			frame.dispose();
		}
	}
	class NewItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			createInterface();
		}
	}
	class OpenItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			createExistingFileFrame();
		}
	}
	class SaveItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			if(lngFile != null && currentFilePath != null) {
				// wyj�cie z trybu edycji
				if(table.isEditing())
					table.getCellEditor().stopCellEditing();
				if(saveIfEmptyRow()) {
					for(int i = 0; i < tableModel.getRowCount(); i++)
						lngFile.setString(i, (String)tableModel.getValueAt(i, STRING_COLUMN));
					lngFile.writeFile(currentFilePath, null);
				}
			}
			else
				JOptionPane.showMessageDialog(null, "B��d w zapisie pliku!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	class SaveAsItemListener implements ActionListener {
		JFrame frame;
		SaveAsItemListener(JFrame frame) {
			this.frame = frame;
		}
		public void actionPerformed(ActionEvent ev) {
			if(lngFile != null) {
				// wyj�cie z trybu edycji
				if(table.isEditing())
					table.getCellEditor().stopCellEditing();
				if(saveIfEmptyRow()) {
					JFileChooser chooser = new JFileChooser();
					chooser.setFileFilter(new FileNameExtensionFilter("LNG file (*.lng)", "lng"));
					int returnVal = chooser.showSaveDialog(frame);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						File file = chooser.getSelectedFile();
						setCurrentFilePath(file.getPath());
						for(int i = 0; i < tableModel.getRowCount(); i++)
							lngFile.setString(i, (String)tableModel.getValueAt(i, STRING_COLUMN));
						lngFile.writeFile(file.getPath() + ".lng", lngCombo.getSelectedItem().toString());
						setFrameTitle(file.getName() + ".lng");
					}
				}
			}
			else
				JOptionPane.showMessageDialog(null, "Nie utworzono pliku!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	// JButton
	class AddActionListener implements ActionListener {
		JFrame frame;
		AddActionListener(JFrame frame) {
			this.frame = frame;
		}
		public void actionPerformed(ActionEvent ev) {
			optionSaveAs.setEnabled(true);
			lngFile.addLNGstring();
			tableModel.addRow(new Object[]{new Integer(0), new String()});
		}
	}
	class DeleteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			try {
				int row = table.getSelectedRow();
				if(row != -1) {
					tableModel.removeRow(row);
					lngFile.deleteString(row);
					// zaznaczenie s�siedniego wpisu
					if(tableModel.getRowCount() > row)
						table.setRowSelectionInterval(row, row);
					else if(row > 0)
						table.setRowSelectionInterval(row - 1, row - 1);
				}
				else {
					if(table.getRowCount() == 0)
						throw new Exception("Tabela jest pusta!");
					else
						throw new Exception("Nie zaznaczono �adnego wpisu!");
				}
				if(table.getRowCount() == 0) {
					optionSave.setEnabled(false);
					optionSaveAs.setEnabled(false);
				}
			} catch(Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	private class DrawEndsStringListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			table.repaint();
		}
	}
	
	private void createExistingFileFrame() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileFilter(new FileNameExtensionFilter("LNG file (*.lng)", "lng"));
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	File file = chooser.getSelectedFile();
 			createInterface();
 			createLNGfile(file.getPath());
 			initTable();
        }
	}
	private void createInterface() {
		if(table == null)
			createNewInterface();
		else
			resetInterface();
	}
	private void createNewInterface() {	
		if(lngFile == null)
			createLNGfile(null);
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		JPanel inputPanel = new JPanel();
		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));
		
		lngCombo = new JComboBox<String>();
		lngCombo.setBounds(500, 50, 100, 20);
		lngCombo.addItem("English");
		lngCombo.addItem("Polski");
		lngCombo.addItem("�e�tina");
		lngCombo.addItem("Deutsch");
		lngCombo.addItem("nieokre�lony");
		lngCombo.setSelectedIndex(1);
		btnPanel.add(lngCombo);

		JButton addButton = new JButton("Dodaj");
		addButton.addActionListener(new AddActionListener(this));
		JButton deleteButton = new JButton("Usu�");
		deleteButton.addActionListener(new DeleteActionListener());
		btnPanel.add(addButton);
		btnPanel.add(deleteButton);
		drawEndsString = new JCheckBox("Poka� kra�ce");
		drawEndsString.addActionListener(new DrawEndsStringListener());
		btnPanel.add(drawEndsString);
		
		inputPanel.add(btnPanel);
		panel.add(inputPanel);
		createTable();
		this.getContentPane().add(BorderLayout.CENTER, panel);
		this.setVisible(true);
	}
	private void createTable() {
		tableModel = getDefaultTableModel();
		table = new JTable();
		prepareTable(tableModel);
		tableScroll = new JScrollPane(table);
		JPanel tablePanel = new JPanel();
		tablePanel.add(tableScroll);
		panel.add(tablePanel);
	}
	private class TableListener implements ComponentListener {
		private int lastRowCount = 0;

		@Override
		public void componentResized(ComponentEvent arg0) {
			// sprawdzenie czy dodano nowy wpis - je�li poprzednia warto�� rowCount jest mniejsza od bie��cej to znaczy, �e tak.
			if(lastRowCount < table.getRowCount()) {
		        table.clearSelection(); // odznaczanie ewentualnie zaznaczonych cells
				// po dodaniu stringu, automatyczne ustawienie si� na jego edycj�
				table.editCellAt(tableModel.getRowCount() - 1, STRING_COLUMN);
				((JTextField) table.getEditorComponent()).grabFocus();
				// przej�cie do ostatnio dodanego rekordu
		        JScrollBar vertical = tableScroll.getVerticalScrollBar();
		        vertical.setValue(vertical.getMaximum());
			}
			lastRowCount = table.getRowCount();
		}

		@Override
		public void componentHidden(ComponentEvent arg0) {}
		@Override
		public void componentMoved(ComponentEvent arg0) {}
		@Override
		public void componentShown(ComponentEvent arg0) {}
	}
	private class TableIDColRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = -6811035794253199720L;

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
			JLabel id = new JLabel(String.valueOf(row + 1));
			return id;
		}
	}
	private class TableStringColRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = 1239512400473884111L;
		JPanel pnl;
		JLabel lbl;
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
			if(drawEndsString.isSelected() && value.toString().trim().length() != value.toString().length()) {
				pnl = new JPanel(new FlowLayout(FlowLayout.LEFT));
				lbl = new JLabel("-->" + value.toString() + "<--");
				lbl.setFont(new Font(table.getFont().getFontName(), Font.BOLD, table.getFont().getSize()));
				if(isSelected)
					pnl.setBackground(table.getSelectionBackground());
				else
					pnl.setBackground(table.getBackground());
				pnl.add(lbl);
				return pnl;
			}
			else {
				pnl = new JPanel(new FlowLayout(FlowLayout.LEFT));
				lbl = new JLabel(value.toString());
				lbl.setFont(new Font(table.getFont().getFontName(), Font.PLAIN, table.getFont().getSize()));
				if(isSelected)
					pnl.setBackground(table.getSelectionBackground());
				else
					pnl.setBackground(table.getBackground());
				pnl.setBorder(BorderFactory.createEmptyBorder());
				lbl.setBorder(BorderFactory.createEmptyBorder());
				pnl.add(lbl);
				return pnl;
			}
		}
	}
	private String checkEmptyRows() {
		boolean noString = true;
		String msg = "";
		for(int i = 0; i < table.getRowCount(); i++)
			if(tableModel.getValueAt(i, STRING_COLUMN).toString().trim().isEmpty()) {
				if(noString) {
					msg = "String: ";
					noString = false;
					msg += (i + 1);
				}
				else
					msg += ", " + (i + 1);
			}
		return msg;
	}
	private boolean saveIfEmptyRow() {
		int option = JOptionPane.YES_OPTION;
		String msg = checkEmptyRows();
		if(!msg.isEmpty())
			option = JOptionPane.showConfirmDialog(null, "Tabela zawiera puste wpisy. Zapisa� plik mimo to?\n" + msg, "Question", JOptionPane.YES_NO_OPTION);
		switch(option) {
		case JOptionPane.YES_OPTION: return true;
		case JOptionPane.NO_OPTION: return false;
		default: return false;
		}
	}
	private DefaultTableModel getDefaultTableModel() {
		DefaultTableModel model = new DefaultTableModel() {
			private static final long serialVersionUID = 8019946646782922381L;
			public Class<?> getColumnClass(int column) {
				switch(column)
				{
				case ID_COLUMN: return Integer.class;
				case STRING_COLUMN: return String.class;
				default: return Object.class;
				}
			}
			public boolean isCellEditable(int rowIndex, int columnIndex) {
				if(columnIndex == STRING_COLUMN)
					return true;
				else
					return false;
			}
		};
		model.addColumn("ID");
		model.addColumn("String");
		return model;
	}
	private void createLNGfile(String path) {
		if(path != null) {
			setFrameTitle(path);
			setCurrentFilePath(path);
			lngFile = new LNGfile(path);
			optionSave.setEnabled(true);
			optionSaveAs.setEnabled(true);
		}
		else {
			setFrameTitle(null);
			setCurrentFilePath(null);
			lngFile = new LNGfile();
			optionSave.setEnabled(false);
			optionSaveAs.setEnabled(false);
		}
	}
	private void initTable() {
		for(int i = 0; i < lngFile.getStringCount(); i++)
			tableModel.addRow(new Object[]{new Integer(0), lngFile.getString(i)});
		lngCombo.setSelectedItem(lngFile.getLanguageLocalName());
	}
	private void prepareTable(DefaultTableModel model) {
		table.setModel(model);
		table.getColumnModel().getColumn(ID_COLUMN).setMaxWidth(MAX_ID_COL_WIDTH);
		table.getColumnModel().getColumn(ID_COLUMN).setPreferredWidth(PREF_ID_COL_WIDTH);
		table.getColumnModel().getColumn(ID_COLUMN).setCellRenderer(new TableIDColRenderer());
		table.getColumnModel().getColumn(STRING_COLUMN).setCellRenderer(new TableStringColRenderer());
		table.setRowHeight(25);
		table.addComponentListener(new TableListener());
	}
	private void resetInterface() {
		currentFilePath = null;
		tableModel = getDefaultTableModel();
		prepareTable(tableModel);
		resetLNGfile();
	}
	private void resetLNGfile() {
		createLNGfile(null);
	}
	private void setCurrentFilePath(String path) {
		currentFilePath = path;
		// zapezpieczenie przed ustawieniem �cie�ki do pliku bez jego rozszerzenia
		if(currentFilePath != null && currentFilePath.lastIndexOf(".lng") != currentFilePath.length() - 4)
			currentFilePath += ".lng";
		if(path == null)
			optionSave.setEnabled(false);
		else
			optionSave.setEnabled(true);
	}
	private void setFrameTitle(String fileName) {
		if(fileName != null) {
			if(fileName.lastIndexOf('\\') != -1)
				fileName = fileName.substring(fileName.lastIndexOf('\\') + 1);
			this.setTitle(defaultFrameTitle + " - " + fileName);
		}
		else
			this.setTitle(defaultFrameTitle);
	}
}
